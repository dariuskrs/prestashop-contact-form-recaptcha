<?php
if (!defined('_PS_VERSION_')) {
    exit;
}
require_once(__DIR__ . '/vendor/autoload.php');

/**
 * Adds a reCAPTCHA to the contact form
 */
class ContactFormReCaptcha extends Module
{
    /**
     * Variable prefix
     */
    private const VARIABLE_PREFIX = "CF_RECAPTCHA_";

    /**
     * Variable declaration
     */
    private const RECAPTCHA_ENABLED = self::VARIABLE_PREFIX."_ENABLED";
    private const RECAPTCHA_PRIVATE_KEY = self::VARIABLE_PREFIX."_PRIVATE_KEY";
    private const RECAPTCHA_PUBLIC_KEY = self::VARIABLE_PREFIX."_PUBLIC_KEY";

    /**
     * Form related configurations
     */
    private const FORM_SUBMIT_ACTION = 'submitContactFormReCaptcha';

    /**
     * Constructor
     */
    public function __construct()
    {
        //ini_set('display_errors', 'on');
        $this->name = 'contactformrecaptcha';
        $this->tab = 'front_office_features';
        $this->version = '0.1.0';
        $this->ps_versions_compliancy = array('min' => '1.7.6.0', 'max' => _PS_VERSION_);
        $this->author = 'Dariuskrs';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Contact form reCAPTCHA', array(), 'Modules.Contactformrecaptcha.Admin');
        $this->dependencies = ['contactform'];
        $this->description = $this->trans('Adds a reCAPTCHA to your contact form.', array(), 'Modules.Contactformrecaptcha.Admin');
    }

    /**
     * 
     */
    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('actionContactFormBeforeMessageSent')
            || !Configuration::updateValue(self::RECAPTCHA_ENABLED, 0)
        ) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        Configuration::deleteByName(self::RECAPTCHA_ENABLED);
        Configuration::deleteByName(self::RECAPTCHA_PRIVATE_KEY);
        Configuration::deleteByName(self::RECAPTCHA_PUBLIC_KEY);

        return true;
    }

    /**
     * 
     */
    public function isUsingNewTranslationSystem()
    {
        return true;
    }

    /**
     * Admin panel configuration
     */
    public function getContent()
    {
        $this->_html .= $this->validateAdminForm();
        $this->_html .= $this->renderAdminForm();

        return $this->_html;
    }

    /**
     * Validates admin values passed on form submission
     */
    public function validateAdminForm() 
    {
        if (Tools::isSubmit(self::FORM_SUBMIT_ACTION)) {
            Configuration::updateValue(self::RECAPTCHA_ENABLED, Tools::getValue(self::RECAPTCHA_ENABLED));
            Configuration::updateValue(self::RECAPTCHA_PRIVATE_KEY, Tools::getValue(self::RECAPTCHA_PRIVATE_KEY));
            Configuration::updateValue(self::RECAPTCHA_PUBLIC_KEY, Tools::getValue(self::RECAPTCHA_PUBLIC_KEY));

            $this->_html .= $this->displayConfirmation($this->trans('reCAPTCHA settings updated', array(), 'Modules.Contactformrecaptcha.SettingsUpdate'));
        }
    }

    /**
     * Renders the admin form to configure the reCAPTCHA
     */
    public function renderAdminForm() 
    {
        $fieldsForm = [
            'form' => [
                'legend' => [
                    'title' => $this->l('reCAPTCHA settings'),
                ],
                'description' => $this->l('Prerequisite: create an account and get public and private keys over at ') . '<br /><a href="https://www.google.com/recaptcha/intro/v3.html" target="_blank">the official Google reCAPTCHA website</a>.',
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => $this->l('Enable the reCAPTCHA on the contact form'),
                        'name' => self::RECAPTCHA_ENABLED,
                        'required' => true,
                        'is_bool' => true,
                        'values' => [
                            [
                                'id' => 'recaptcha_yes',
                                'value' => 1,
                                'label' => $this->l('Enabled'),
                            ],
                            [
                                'id' => 'recaptcha_no',
                                'value' => 0,
                                'label' => $this->l('Disabled'),
                            ],
                        ]
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('reCAPTCHA public/"site" key'),
                        'name' => self::RECAPTCHA_PUBLIC_KEY,
                        'required' => true,
                        'empty_message' => $this->l('The public key is needed for the reCPATCHA to work'),
                        'tab' => 'general',
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('reCAPTCHA private/"secret" key'),
                        'name' => self::RECAPTCHA_PRIVATE_KEY,
                        'required' => true,
                        'empty_message' => $this->l('The private key is needed for the reCPATCHA to work'),
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                ]
            ],
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        // Language
        $defaultLang = (int) Configuration::get('PS_LANG_DEFAULT');
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = self::FORM_SUBMIT_ACTION;

        // Populate admin form
        $this->populateAdminForm($helper);

        return $helper->generateForm([$fieldsForm]);
    }

    /**
     * @param HelperForm $helper
     */
    private function populateAdminForm($helper) 
    {
        $helper->fields_value[self::RECAPTCHA_ENABLED] = Configuration::get(self::RECAPTCHA_ENABLED);
        $helper->fields_value[self::RECAPTCHA_PUBLIC_KEY] = Configuration::get(self::RECAPTCHA_PUBLIC_KEY);
        $helper->fields_value[self::RECAPTCHA_PRIVATE_KEY] = Configuration::get(self::RECAPTCHA_PRIVATE_KEY);
    }

    /**
     * Hook into the header info
     */
    public function hookDisplayHeader(array $params)
    {
        if ($this->context->controller != null &&
            $this->context->controller instanceof ContactController) 
            {
                // Link to module CSS
                $this->context->controller->registerStylesheet(
                    'module-'.$this->name,
                    'modules/'.$this->name.'/views/assets/css/styles.css'
                );

                // Link to Google reCAPTCHA
                $this->context->smarty->assign(
                    array(
                       'recaptchaPublicKey' => Configuration::get(self::RECAPTCHA_PUBLIC_KEY),
                    )
                 );

                return $this->display(__FILE__, 'views/headers.tpl');
            }
    }

    /**
     * Intercept captcha infos on contact form after submit 
     * but before persisting and sending a message
     */
    public function hookActionContactFormBeforeMessageSent() 
    {
        if (Configuration::get(self::RECAPTCHA_ENABLED) == 1 && Tools::isSubmit('submitMessage')) 
        {
            $context = Context::getContext();
            $captcha = new \ReCaptcha\ReCaptcha(Configuration::get(self::RECAPTCHA_PRIVATE_KEY));
            
            $result = $captcha->verify(Tools::getValue('recaptcha-response'),
                Tools::getRemoteAddr());

            if (!$result->isSuccess()) {
                $errorMessage = $this->l('You were identified as an untrusted client. Please try again.');
                $context->controller->errors[] = $errorMessage;
                return false;
            }
        }
    }

}
