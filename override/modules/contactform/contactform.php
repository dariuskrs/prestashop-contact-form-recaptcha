<?php
/**
 * Hook before sending and persisting the customer's message
 */
class ContactformOverride extends Contactform
{
    public function sendMessage() {
        //Module Eicaptcha : Check captcha before submit
        Hook::exec('actionContactFormBeforeMessageSent');
        if (empty($this->context->controller->errors)) {
            parent::sendMessage();
        }
     }
}