# Contact form reCAPTCHA - A Prestashop module

A module for Prestashop to add a reCAPTCHA v3 to the contact form of Prestashop websites.

## Prerequisites
Prestashop 1.7.6.x 
The "Contact form module"
In this early stage, has only been tested on 1.7.6.2

## Install
Clone the Git repository.
With composer (install it if you don't already have it), go into the cloned directory, then
``
composer install
``
You may then zip the module and distribute it / import it.

## Note
Currently, this is a very early version: it simply does what it says, that is:
- Apply a captcha (reCAPTCHA v3) to the contact form of Prestashop websites
- Allow the user to configure private and public keys in the Admin panel

## Versions
- 0.1.0: initial version; does the basics, code still needs a lot of cleaning up

## Todo List
- Allow the user to choose the theme of the reCAPTCHA
- Clean injection of the recaptcha related divs, currently done with DOM manipulation, should use Hooks instead ideally
- Rework the code per PHP-FIG recommendations
- Give more control to the user when it comes to styling the small reCAPTCHA box

## License model
GNU General Public License v3.0