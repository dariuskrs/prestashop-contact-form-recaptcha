{literal}
<script async src="https://www.google.com/recaptcha/api.js?render={/literal}{$recaptchaPublicKey}{literal}"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){ 
    let formWrapper = document.querySelector(".form-fields");
    formWrapper.insertAdjacentHTML('beforeend', 
    '<div id="recaptcha-wrapper"><input type="hidden" name="recaptcha-response" id="recaptcha-response"></div>');

    grecaptcha.ready(function() {
        grecaptcha.execute('{/literal}{$recaptchaPublicKey}{literal}', {action: 'contact'}).then(function(token) {
            let recaptchaResponse = document.getElementById('recaptcha-response');
            recaptchaResponse.value = token;
        });
    });
}, false);
</script>
{/literal}